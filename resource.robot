*** Settings ***
Documentation   Template robot main suite.
Library    RPA.Browser

*** Variables ***
${random_string} =   Generate Random String    12    [LOWER]
${BROWSER URL}    https://qa.dth-events.live/

#Home Page
${SIGNIN BUTTON}    //*[@id="body"]/div/div/div[3]/div/div/a
${SIGNUP BUTTON}   //*[@id="body"]/div/div/div[3]/div/div/a[2]/span

#Login Page
${USERNAME FIELD}    //*[@id="id_username"]
${PASSWORD FIELD}    //*[@id="id_password"]
${ENTER BUTTON}    //*[@id="body"]/div/div/div[2]/main/div/form/div[4]/button/span
${CREATE ACCOUNT BUTTON}      //*[@id="body"]/div/div/div[2]/main/div/p/a[1]
${RESET PASSWORD BUTTON}     //*[@id="body"]/div/div/div[2]/main/div/p/a[2]

#Create Account Page
${CAP SIGNIN BUTTON}    //*[@id="body"]/div/div/div[2]/main/div/p/a[1]
${CAP RESET PASSWORD BUTTON}    //*[@id="body"]/div/div/div[2]/main/div/p/a[2]
${CAP CREATE ACCOUNT BUTTON}    //*[@id="createAccountForm"]/div[8]/button
#Create account Tests
${FIRST NAME}    //*[@id="id_first_name"]
${LAST NAME}    //*[@id="id_last_name"]
${ORGANISATION}    //*[@id="id_organisation"]
${EMAIL}    //*[@id="id_email"]
${PASSWORD}    //*[@id="id_password1"]
${CONFIRM PASSWORD}     //*[@id="id_password2"] 
${TERMS OF USE CHECKBOX}     //*[@id="id_terms_and_conditions"]

#Reset Password Page 
${EMAIL FIELD}     //*[@id="id_email"]
${START RESET BUTTON}     //*[@id="body"]/div/div/div[2]/main/div/form/div[2]/button

${VALID USERMANE}    karl@hci.digital
${VALID PASSWORD}    l3tm31nn0w!
${INVALID USERNAME}    Jack@hci.digital
${INVALID PASSWORD}    SDHHDHJSJCKJB




*** Keywords ***
Navigate to the DOE site 
    Open Available Browser     ${BROWSER URL}
#login Tests 
navigate to login page   
    Open Available Browser     ${BROWSER URL}
    Click Element When Visible    ${SIGNIN BUTTON} 

Select create account 
    Click Element    ${CREATE ACCOUNT BUTTON}

Select reset password 
    Click Element    ${RESET PASSWORD BUTTON}
    
Enter login details - succeful
    Input Text When Element Is Visible    ${USERNAME FIELD}   ${VALID USERMANE}
    Input Text When Element Is Visible    ${PASSWORD FIELD}   ${VALID PASSWORD}
    Click Element    ${ENTER BUTTON}
Enter login details - invalid email
    Input Text When Element Is Visible    ${USERNAME FIELD}   ${INVALID USERNAME}
    Input Text When Element Is Visible    ${PASSWORD FIELD}   ${VALID PASSWORD}
    Click Element    ${ENTER BUTTON}

Enter login details - Special Character
    Input Text When Element Is Visible    ${USERNAME FIELD}    k@rl@hci.digital
    Input Text When Element Is Visible    ${PASSWORD FIELD}    ${VALID PASSWORD}
    Click Element    ${ENTER BUTTON}
     
    

Enter login details - Missing Password
    Input Text When Element Is Visible    ${USERNAME FIELD}    ${VALID USERMANE}
    Input Text When Element Is Visible    ${PASSWORD FIELD}    
    Click Element    ${ENTER BUTTON}

Navigate to the create an account page 
    Open Available Browser     ${BROWSER URL}
    Click Element When Visible    ${SIGN UP BUTTON}
    

#Reset Password Tests 
Enter the emails for the account you wish to reset the password for
    Input Text When Element Is Visible    ${EMAIL FIELD}   ${INVALID USERNAME} 

#Create Account Test
Fill sign up Form 
#Random string generator needed for this to be tested properly
   Input Text    ${FIRST NAME}    test
   Input Text    ${LAST NAME}    test
   Input Text    ${ORGANISATION}    test inc
   Input Text    ${EMAIL}    test@test.com
   Input Text    ${PASSWORD}    testwordtest
   Input Text    ${CONFIRM PASSWORD}     testwordtest
    Select Checkbox    ${TERMS OF USE CHECKBOX}     

    

   

    





   
