*** Settings ***
Documentation   Template robot main suite.
Resource     resource.robot

*** Tasks ***  
complete a succeful Log in 
    Log  Done.
    navigate to login page
    Enter login details - succeful test
    Screenshot
    Close All Browsers

complete a succeful Log in - Via the Sign up Page
    Log  Done.
    Navigate to the DOE site
    Click Element    ${SIGNUP BUTTON}
    Click Element When Visible       ${CAP SIGNIN BUTTON}
    Enter login details - succeful
    Screenshot
    Close All Browsers

complete a unsucceful Log in 
    Log  Done.
    navigate to login page
    Enter login details - invalid email    
    Select create account 
    Screenshot
    Close All Browsers

complete a unsucceful Log in Special Character
    Log  Done.
    navigate to login page
    Enter login details - Special Character
    Select create account
    Screenshot
    Close All Browsers



Reset Password from login page - unregistered email
    Log  Done.
    navigate to login page
    Select reset password
    Enter the emails for the account you wish to reset the password for    
    Click Element    ${START RESET BUTTON}
    Page Should Contain Element    ${START RESET BUTTON}
    Close All Browsers
Reset Password from login page - registered email
#currently not working due to gateway error
    Log  Done.
    navigate to login page
    Select reset password
    Enter the emails for the account you wish to reset the password for    
    Click Element    ${START RESET BUTTON}

    Close All Browsers


Create an account - unsucceful
   Navigate to the create an account page
   Fill sign up Form
   Click Element    ${CAP CREATE ACCOUNT BUTTON}
